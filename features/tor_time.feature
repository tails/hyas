@product
Feature: Tor survives time jumps

  Background:
    Given a computer
    And the network is unplugged
    And I start the computer
    And the computer boots Tails
    And I log in to a new session
    And GNOME has started
    And Tor "0.2.5.5-alpha" is installed
    And Tor has debug logging enabled
    And I set the system time to "now - 5 days"
    And I save the state so the background can be restored next scenario

  Scenario: 1
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 2
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 3
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 4
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 5
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 6
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 7
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 8
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 9
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 10
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 11
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 12
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 13
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 14
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 15
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 16
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 17
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 18
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 19
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully

  Scenario: 20
    When the network is plugged
    And Tor is running
    And Tor notices the clock is off
    And Tails asks Tor for the time
    #And I wait between 5 and 10 seconds
    And Tor magically syncs the time
    And Tor notices that your clock jumped
    Then Tor bootstraps successfully
