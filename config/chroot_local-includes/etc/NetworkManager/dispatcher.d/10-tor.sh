#!/bin/sh

# We don't start Tor automatically so *this* is the time
# when it is supposed to start.

# Run only when the interface is not "lo":
if [ $1 = "lo" ]; then
   exit 0
fi

# Run whenever an interface gets "up", not otherwise:
if [ $2 != "up" ]; then
   exit 0
fi

# Import tor_control_setconf(), TOR_LOG
. /usr/local/lib/tails-shell-library/tor.sh

# Import tails_netconf()
. /usr/local/lib/tails-shell-library/tails_greeter.sh

if ! service tor status; then
    service tor start
#elif ! tor_is_working; then
#    service tor restart
fi

if [ "$(tails_netconf)" = "obstacle" ]; then
   /usr/local/sbin/tails-tor-launcher &
fi
