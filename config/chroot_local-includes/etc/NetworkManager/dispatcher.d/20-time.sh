#!/bin/sh

# Rationale: Tor needs a somewhat accurate clock to work.
# If the clock is wrong enough to prevent it from opening circuits,
# we set the time to the middle of the valid time interval found
# in the Tor consensus, and we restart it.
# In any case, we use HTP to ask more accurate time information to
# a few authenticated HTTPS servers.

# Get LIVE_USERNAME
. /etc/live/config.d/username.conf

# Import tor_control_*(), tor_is_working(), TOR_LOG, TOR_DIR
. /usr/local/lib/tails-shell-library/tor.sh

# Import tails_netconf()
. /usr/local/lib/tails-shell-library/tails_greeter.sh

### Exit conditions

# Run only when the interface is not "lo":
if [ "$1" = "lo" ]; then
	exit 0
fi

# Run whenever an interface gets "up", not otherwise:
if [ "$2" != "up" ]; then
	exit 0
fi

### Functions

log() {
	logger -t time "$@"
}

is_clock_way_off() {
	log "Checking if system clock is way off"
	until [ "$(tor_bootstrap_progress)" -gt 10 ]; do
		if tor_cert_lifetime_invalid; then
			return 0
		fi
		sleep 1
	done
	return 1
}

### Main

# When the network is obstacled (e.g. we need a bridge) we wait until
# Tor Launcher has unset DisableNetwork, since Tor's bootstrapping
# won't start until then.
if [ "$(tails_netconf)" = "obstacle" ]; then
	until [ "$(tor_control_getconf DisableNetwork)" = 0 ]; do
		sleep 1
	done
fi

touch /waiting_for_time_sync
while true; do
    if [ -e /time_is_synced ]; then
        break
    fi
    sleep 1
done

wait_for_working_tor

log "Restarting htpdate"
service htpdate restart
log "htpdate service restarted with return code $?"
