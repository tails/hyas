[[!meta title="Start Tails"]]

[[!toc]]

Now that you have a Tails device you can shutdown your computer and start using
Tails without altering your existing operating system.

If you are using a DVD
======================

Put the Tails DVD into the CD/DVD-drive and restart the computer. You should see
a welcome screen prompting you to choose your language.

If you don't get this menu, you can consult the Ubuntu documentation about
[booting from the CD](https://help.ubuntu.com/community/BootFromCD) for more
information, especially the part on the [BIOS
settings](https://help.ubuntu.com/community/BootFromCD#BIOS%20is%20not%20set%20to%20boot%20from%20CD%20or%20DVD%20drive).

If you are using a USB stick or SD card
=======================================

Shutdown the computer, plug your device and start the computer. You should see a
welcome screen prompting you to choose your language.

If your computer does not automatically do so, you might need to edit the BIOS
settings. Restart your computer, and watch for a message telling you which key
to press to enter the BIOS setup. It will usually be one of F1, F2, DEL, ESC or
F10. Press this key while your computer is booting to edit your BIOS settings.
You need to edit the Boot Order.  Depending on your computer you should see an
entry for 'removable drive' or 'USB media'. Move this to the top of the list to
force the computer to attempt to start from your device before starting from the
internal hard disk. Save your changes and continue.

For more detailed instruction on how to boot from USB you can read [About.com:
How To Boot your Computer from a Bootable USB
Device](http://pcsupport.about.com/od/tipstricks/ht/bootusbflash.htm).

If you have problems accessing the BIOS, try to read [pendrivelinux.com: How to
Access BIOS](http://www.pendrivelinux.com/how-to-access-bios/).

Problems starting Tails?
========================

Read our specific [[reporting guidelines when Tails does not
start|doc/first_steps/bug_reporting/tails_does_not_start]].

Using a virtualization software
===============================

[[See the corresponding documentation.|advanced_topics/virtualization]]
